long          cMas[10];
unsigned char i;

int main()
{
	i = 0;
	do
	{
		if (i <= 7)
			cMas[i] = (17ul * i) & 0x0Eu;
		else
			cMas[i] = 25 * i / 4;

		++i;
	} while (i < 9);

	return 0;
}
