000011b4 <Fn>:

unsigned char Fn(unsigned char foo, char bar)
{
; Стандартная преамбула
    11b4:	55            	push   ebp
    11b5:	89 e5         	mov    ebp,esp
; Отступить место на стеке для локальных переменных
    11b7:	83 ec 18      	sub    esp,0x18
; gcc-specific
    11ba:	e8 2b 00 00 00	call   11ea <__x86.get_pc_thunk.ax>
    11bf:	05 41 2e 00 00	add    eax,0x2e41
; Записать в edx ebp со смещением в 8 байт (первый аргумент)
    11c4:	8b 55 08      	mov    edx,DWORD PTR [ebp+0x8]
; Записать в eax ebp со смещением в 12 байт (второй аргумент)
    11c7:	8b 45 0c      	mov    eax,DWORD PTR [ebp+0xc]
; Записать сохраненные данные на стек
; c отступом в 0xC (12) байт от предыдущих
    11ca:	88 55 ec      	mov    BYTE PTR [ebp-0x14],dl
    11cd:	88 45 e8      	mov    BYTE PTR [ebp-0x18],al

	int           a = bar;
; Скопировать второй аргумент в eax
    11d0:	0f be 45 e8   	movsx  eax,BYTE PTR [ebp-0x18]
; Записать в локальную переменную a (четырехбайтовая)
    11d4:	89 45 fc      	mov    DWORD PTR [ebp-0x4],eax
	unsigned char b = foo;
; Скопировать первый аргумент в eax
    11d7:	0f b6 45 ec   	movzx  eax,BYTE PTR [ebp-0x14]
; Записать в локальную переменную b (однобайтовая)
    11db:	88 45 fb      	mov    BYTE PTR [ebp-0x5],al

	return foo + bar;
; Скопировать первый и второй аргументы
; в eax и edx соответственно
    11de:	0f b6 55 e8   	movzx  edx,BYTE PTR [ebp-0x18]
    11e2:	0f b6 45 ec   	movzx  eax,BYTE PTR [ebp-0x14]
; Прибавить к аргументу в eax второй.
; Сохраненный в eax результат является возвращаемым значением.
    11e6:	01 d0         	add    eax,edx
    11e8:	c9            	leave
    11e9:	c3            	ret
