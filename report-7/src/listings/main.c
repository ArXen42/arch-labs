unsigned char Fn(unsigned char, char);

int main()
{
	Fn(2, 4);
}

unsigned char Fn(unsigned char foo, char bar)
{
	// В функции объявите и используйте локальную переменную типа int и еще одну типа unsigned char.

	int           a = bar;
	unsigned char b = foo;

	return foo + bar;
}
