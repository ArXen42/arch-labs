0000117d <main>:
unsigned char Fn(unsigned char, char);

int main()
{
; gcc-specific, сдвинуть стековый кадр до следуюшего адреса,
; выровненного до 16 байт
; указатель на аргументы функции при этом сохранен в ecx
    117d:	8d 4c 24 04    	lea    ecx,[esp+0x4]
    1181:	83 e4 f0       	and    esp,0xfffffff0
    1184:	ff 71 fc       	push   DWORD PTR [ecx-0x4]
; Стандартная преамбуда
    1187:	55             	push   ebp
    1188:	89 e5          	mov    ebp,esp
    118a:	51             	push   ecx
    118b:	83 ec 04       	sub    esp,0x4
    118e:	e8 57 00 00 0  	call   11ea <__x86.get_pc_thunk.ax>
    1193:	05 6d 2e 00 0  	add    eax,0x2e6d
	Fn(2, 4);
; Вызов функции Fn (согласно cdecl):
; Отступить место для аргументов
    1198:	83 ec 08       	sub    esp,0x8
; Вставить в стек второй аргумент
    119b:	6a 04          	push   0x4
; Вставить в стек первый аргумент
    119d:	6a 02          	push   0x2
; Вызов процедуры
    119f:	e8 10 00 00 00 	call   11b4 <Fn>
; Удалить аргументы из стекового кадра (0x10 из-за выравнивания)
    11a4:	83 c4 10       	add    esp,0x10
; Записать возвращаемое значение 0 в регистр eax
    11a7:	b8 00 00 00 00 	mov    eax,0x0
}
; Восстановить стековый кадр
    11ac:	8b 4d fc       	mov    ecx,DWORD PTR [ebp-0x4]
    11af:	c9             	leave
    11b0:	8d 61 fc       	lea    esp,[ecx-0x4]
    11b3:	c3             	ret